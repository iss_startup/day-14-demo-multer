module.exports = function(app){

    var fs = require("fs");
    var path = require("path");

    var multer = require("multer");
    //var multipart = multer({dest: path.join(__dirname, "/upload_tmp/")});

    var storage = multer.diskStorage({
        destination: './uploads_tmp/',
        filename: function (req, file, cb) {
           // console.log(file)
            cb(null, file.originalname)
        }
    })

    var multipart = multer({ storage: storage });

    app.post("/upload", multipart.single("img-file"), function (req, res) {
        console.log(req.file);
        fs.readFile(req.file.path, function (err, data) {
            res.send(202,req.file.size);
        });
    });

}
