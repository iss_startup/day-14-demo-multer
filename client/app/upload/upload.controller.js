(function(){
    angular
        .module("UploadApp")
        .controller("UploadCtrl", UploadCtrl);

    UploadCtrl.$inject = ["Upload"];

    function UploadCtrl(Upload) {
        var vm = this;
        vm.imgFile = null;
        vm.comment = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.content = []; // variable that holds filenames returned from server

        vm.upload = function () {

            Upload.upload({
                url: '/upload',
                data:  {
                    "img-file": vm.imgFile,
                    "comment": vm.comment
                }
            }).then(function (resp) {
                vm.fileurl = resp.data;
                vm.status.message = "The image is saved successfully with size : " + Math.round(resp.data/1024) +'Kilo Bites';
                vm.status.code = 202;
            }).catch(function (err) {
                vm.status.message = "Fail to save the image.";
                vm.status.code = 400
            });

        };
    }
})();
